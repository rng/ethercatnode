ETHIF = enp1s0f0

ARCHFLAGS =  -mthumb -march=armv7e-m -mfpu=fpv4-sp-d16 -mfloat-abi=hard
OPT=s

COMMONCFLAGS = $(ARCHFLAGS) \
               -O$(OPT) -g -DSTM32F3 -Wall -Werror -Wextra \
               -Wdouble-promotion -fsingle-precision-constant -Wno-implicit-fallthrough\
               -fno-common -ffunction-sections -fdata-sections --specs=nano.specs \
               -Wredundant-decls -MMD -MP \
               -Isrc \
               -Ilib/libopencm3/include \
               -Ilib/SOES/ \
               -Ilib/tinyprintf/ \
               -DUSE_ULOG=1 \

CFLAGS = -std=gnu11 $(COMMONCFLAGS)

LDFLAGS = $(ARCHFLAGS) \
          -Llib/libopencm3/lib -Lld -lm -lopencm3_stm32f3 --static -nostartfiles \
          -Wl,--gc-sections \

BINDIR = build

LIBECAT_SRCS = src/ethercat.c \
               src/ethercat_hw.c \
               lib/SOES/soes/esc.c \
               lib/SOES/soes/esc_foe.c \

MAIN_SRCS = src/main.c \
            src/flash.c \
            src/a1335.c \
            src/board.c \
            src/ulog.c \
            src/drv8305.c \
            src/pwm.c \
            lib/tinyprintf/tinyprintf.c \

BOOT_SRCS = src/bootloader.c \
            src/flash.c \

LIBECAT_OBJS = $(LIBECAT_SRCS:%.c=$(BINDIR)/%.o)
MAIN_OBJS = $(MAIN_SRCS:%.c=$(BINDIR)/%.o)
BOOT_OBJS = $(BOOT_SRCS:%.c=$(BINDIR)/%.o)

OBJS = $(MAIN_OBJS) $(BOOT_OBJS) $(LIBECAT_OBJS)

DEPS = $(OBJS:%.o=%.d)
TARGET = ethercatnode

all: ethercatnode.fw \
     ethercatnode-a.lst ethercatnode-b.lst \
     bootloader.bin bootloader.lst

-include $(DEPS)

$(BINDIR)/libecat.a: $(LIBECAT_OBJS)

src/build.h:
	@echo "[HDR]     $@"
	@./tools/genhdr.py >> $@

src/main.c: src/build.h

bootloader.elf: $(BOOT_OBJS) lib/libopencm3/lib/libopencm3_stm32f3.a
	@echo "[LD]      $@"
	@arm-none-eabi-gcc $^ $(LDFLAGS) -Tstm32f301r8_bl.ld -o $@

ethercatnode-a.elf: $(MAIN_OBJS) $(BINDIR)/libecat.a lib/libopencm3/lib/libopencm3_stm32f3.a
	@echo "[LD]      $@"
	@arm-none-eabi-gcc $^ $(LDFLAGS) -Tstm32f301r8_a.ld -o $@

ethercatnode-b.elf: $(MAIN_OBJS) $(BINDIR)/libecat.a lib/libopencm3/lib/libopencm3_stm32f3.a
	@echo "[LD]      $@"
	@arm-none-eabi-gcc $^ $(LDFLAGS) -Tstm32f301r8_b.ld -o $@

lib/libopencm3/lib/libopencm3_stm32f3.a:
	make -C lib/libopencm3 all

%.bin: %.xml
	@echo "[SII]     $@"
	@siitool -o $@ $<

$(BINDIR)/%.a:
	@echo "[AR]      $@"
	@mkdir -p $(dir $@)
	@arm-none-eabi-ar rcs $@ $^

$(BINDIR)/%.o: %.c
	@echo "[CC]      $@"
	@mkdir -p $(dir $@)
	@arm-none-eabi-gcc $(CFLAGS) -o $@ -c $<

%.bin: %.elf
	@echo "[OBJCOPY] $@"
	@arm-none-eabi-objcopy -Obinary $< $@

%.lst: %.elf
	@arm-none-eabi-objdump -d $< > $@

%.fw: %-a.bin %-b.bin
	@echo "[MAKEFW]  $@"
	@./tools/makefw.py $^ $@

debugserver:
	openocd -f ocd/target.cfg

debug: ethercatnode-a.elf ethercatnode-b.elf bootloader.elf
	arm-none-eabi-gdb -x ocd/gdb $<

eepromflash: data/eeprom.bin
	@echo "[EEFLASH] $<"
	@eepromtool $(ETHIF) 1 -w $<

clean:
	@echo "[CLEAN]"
	@rm -rf $(OBJS) $(DEPS) $(BINDIR) *.bin *.elf *.lst *.a *.fw src/build.h

term:
	miniterm /dev/ttyUSB0 460800

.PHONY: test
test:
	@./test.py test/test_ethercat_hw.cpp

format:
	clang-format -i src/*.c src/*.h

wc:
	wc -l src/*.[ch] | sort -n
