#!/usr/bin/env python

import subprocess
import tempfile
import sys
import re
import os

testfile = sys.argv[1]

testmain = tempfile.NamedTemporaryFile(suffix=".cpp")
testbin = tempfile.NamedTemporaryFile(delete=False)
testbin.close()

testsrc = open(testfile).read()
mocks = re.findall("MOCK\((.*)\)", testsrc)
tests = re.findall("TEST\((.*)\)", testsrc)
fm = open(testmain.name,"wt")
fm.write("#include <stdio.h>\n")
fm.write("#include <stdint.h>\n")
fm.write("#include \"minimock.h\"\n\n")

fm.write("extern \"C\" {\n")
for mock in mocks:
    fm.write("%s {\n" % mock)
    rett = mock.split()[0]
    args = []
    for arg in mock[:-1].split("(")[1].split(","):
        if "..." not in arg:
            args.append(arg.split()[-1])
    pf, rf = "", "r"
    if rett != "void":
        pf, rf = "return ", "r_uint"
    fm.write(pf + "CALL()" + ("".join(".w(%s)" % a for a in args))  + (".%s();\n" % rf))
    fm.write("}\n")
fm.write("}\n")

for test in tests:
    fm.write("extern void %s();\n" % test)
fm.write("int main() {\n")
for test in tests:
    fm.write("printf(\"%s\\n\");\n" % test)
    fm.write("%s();\n" % test)
fm.write("return 0;\n")
fm.write("}")
fm.close()

flags = "-g -Werror -DSTM32F3 -Itest -Ilib/SOES -Ilib/libopencm3/include -Isrc".split()

def comp_c(fn, flags):
    ofn = tempfile.NamedTemporaryFile(delete=False)
    subprocess.check_call(["gcc"] + flags + ["-o", ofn.name, "-c", fn])
    return ofn.name

def comp_cpp(fn, flags):
    ofn = tempfile.NamedTemporaryFile(delete=False)
    subprocess.check_call(["g++"] + flags + ["-std=c++17", "-fexceptions", "-o", ofn.name, "-c", fn])
    return ofn.name

fa = comp_c("src/ethercat_hw.c", flags)
fb = comp_cpp("test/minimock.cpp", flags)
fc = comp_cpp(testfile, flags)
fd = comp_cpp(testmain.name, flags)
subprocess.check_call(["g++"]+flags+["-std=c++17", "-fexceptions", "-o", testbin.name, fa, fb, fc, fd])
os.unlink(fa)
os.unlink(fb)
os.unlink(fc)
os.unlink(fd)

subprocess.check_call([testbin.name])
os.unlink(testbin.name)
