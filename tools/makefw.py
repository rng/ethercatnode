#!/usr/bin/env python

import sys


def main():
    if len(sys.argv) != 4:
        print "usage: %s <fw-a.bin> <fw-b.bin> <output.fw>" % sys.argv[0]
        sys.exit(-1)

    fwafn, fwbfn, outfn = sys.argv[1:]
    fwa = open(fwafn, "rb").read()
    fwb = open(fwbfn, "rb").read()
    assert len(fwa) == len(fwb), "Firmware sizes do not match"

    with open(outfn, "wb") as f:
        f.write(fwa)
        f.write(fwb)


main()
