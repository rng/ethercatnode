#!/usr/bin/env python

import subprocess
import time

sha = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip()[:8]
unixtime = int(time.time())

TEMPLATE = """#include "app_header.h"

__attribute__((section(".apphdr")))
app_header_t app_header = {
    .magic = APP_MAGIC_ID,
    .buildtime = %d,
    .buildsha = "%s",
    .buildversion = APP_VERSION,
};"""

print TEMPLATE % (unixtime, sha)
