#include "minimock.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <deque>


struct Expect {
    const char *_fn;
    std::vector<std::any> _args;
    std::any _ret;

    Expect(const char *fn, std::vector<std::any> args) {
        _fn = fn;
        _args = args;
    }

    Expect(const char *fn, std::vector<std::any> args, std::any ret) {
        _fn = fn;
        _args = args;
        _ret = ret;
    }
};


std::deque<Expect> expects;


Mock& Mock::call(const char *fn) {
    _fn = fn;
}

Mock& Mock::expect(const char *fn) {
    _fn = fn;
    _expectation = true;
}

Mock& Mock::w(uint32_t v) {
    _args.push_back(v);
    return *this;
}

Mock& Mock::w(const char *v) {
    _args.push_back(v);
    return *this;
}

Expect _check(const char *fn, std::vector<std::any> args) {
    if (!expects.size()) {
        printf("no more expects for %s\n", fn);
        throw;
    }

    auto exp = expects.front();

    if (strcmp(exp._fn, fn) != 0) {
        throw "functions don't match";
    }
    if (exp._args.size() != args.size()) {
        throw "function args don't match";
    }
    for (int i = 0; i < args.size(); i++) {
        auto ca = args[i];
        auto ea = exp._args[i];
        assert(ea.type() == ca.type());
        if (ea.type() == typeid(uint32_t)) {
            auto cv = std::any_cast<uint32_t>(ca);
            auto ev = std::any_cast<uint32_t>(ea);
            if (cv != ev) {
                printf("argument mismatch to %s: %x != %x\n", fn, cv, ev);
                throw;
            }
        }
    }
    expects.pop_front();
    return exp;
}

void Mock::r() {
    _check(_fn, _args);
}

uint32_t Mock::r_uint() {
    auto exp = _check(_fn, _args);
    assert(exp._ret.has_value());
    uint32_t v = std::any_cast<uint32_t>(exp._ret);
    return v;
}

void Mock::done() {
    expects.push_back(Expect(_fn, _args));
}

void Mock::returning(uint32_t v) {
    expects.push_back(Expect(_fn, _args, v));
}
