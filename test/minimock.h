#pragma once

#include <stdint.h>

#include <any>
#include <vector>

#define TEST(n) void n()
#define MOCK(...)
#define CALL() Mock().call(__FUNCTION__)

class Mock {
  private:
    bool _expectation = false;
    const char *_fn;
    std::vector<std::any> _args;

  public:
    Mock &call(const char *fn);
    Mock &expect(const char *fn);
    Mock &w(uint32_t v);
    Mock &w(const char *v);

    void r();
    uint32_t r_uint();

    // expectations
    void done();
    void returning(uint32_t v);
};
