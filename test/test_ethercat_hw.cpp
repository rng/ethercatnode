#include "minimock.h"
#include <string.h>
#include <stdio.h>

extern "C" {

#include "board.h"
#include "ethercat_hw.h"

MOCK(uint8_t spi_read8(uint32_t spi));
MOCK(void spi_send8(uint32_t spi, uint8_t data));
MOCK(void msleep(uint32_t delay));
MOCK(void ulog_printf(const char *format, ...));
MOCK(void gpio_clear(uint32_t gpioport, uint16_t gpios));
MOCK(void gpio_set(uint32_t gpioport, uint16_t gpios));

_ESCvar ESCvar;
}

// tests

auto expect_spi_xfer(uint8_t w, uint8_t r = 0) {
    Mock().expect("spi_send8").w(ECAT_SPI).w(w).done();
    Mock().expect("spi_read8").w(ECAT_SPI).returning(r);
}

auto expect_write32(uint16_t addr, uint32_t val) {
    Mock().expect("gpio_clear").w(GPIOB).w(GPIO12).done();
    expect_spi_xfer(ESC_CMD_SERIAL_WRITE);
    expect_spi_xfer((addr >> 8) & 0xff);
    expect_spi_xfer(addr & 0xff);
    expect_spi_xfer(val & 0xff);
    expect_spi_xfer((val >> 8) & 0xff);
    expect_spi_xfer((val >> 16) & 0xff);
    expect_spi_xfer((val >> 24) & 0xff);
    Mock().expect("gpio_set").w(GPIOB).w(GPIO12).done();
}

auto expect_writebuf(uint16_t addr, uint8_t *buf, size_t len) {
    Mock().expect("gpio_clear").w(GPIOB).w(GPIO12).done();
    expect_spi_xfer(ESC_CMD_SERIAL_WRITE);
    expect_spi_xfer((addr >> 8) & 0xff);
    expect_spi_xfer(addr & 0xff);
    for (int i=0;i<len;i++) {
        expect_spi_xfer(buf[i]);
    }
    Mock().expect("gpio_set").w(GPIOB).w(GPIO12).done();
}

auto expect_read32(uint16_t addr, uint32_t val) {
    Mock().expect("gpio_clear").w(GPIOB).w(GPIO12).done();
    expect_spi_xfer(ESC_CMD_SERIAL_READ);
    expect_spi_xfer((addr >> 8) & 0xff);
    expect_spi_xfer(addr & 0xff);
    expect_spi_xfer(0, val & 0xff);
    expect_spi_xfer(0, (val >> 8) & 0xff);
    expect_spi_xfer(0, (val >> 16) & 0xff);
    expect_spi_xfer(0, (val >> 24) & 0xff);
    Mock().expect("gpio_set").w(GPIOB).w(GPIO12).done();
}

auto expect_readbuf(uint16_t addr, uint8_t *buf, size_t len) {
    Mock().expect("gpio_clear").w(GPIOB).w(GPIO12).done();
    expect_spi_xfer(ESC_CMD_SERIAL_READ);
    expect_spi_xfer((addr >> 8) & 0xff);
    expect_spi_xfer(addr & 0xff);
    for (int i=0;i<len;i++) {
        expect_spi_xfer(0, buf[i]);
    }
    Mock().expect("gpio_set").w(GPIOB).w(GPIO12).done();
}

auto expect_write_csr(uint16_t addr, uint8_t *buf, size_t len) {
    uint32_t value = 0;
    memcpy((uint8_t *)&value, buf, len);
    expect_write32(ESC_CSR_DATA_REG, value);
    expect_write32(ESC_CSR_CMD_REG,
                   ESC_CSR_CMD_WRITE | ESC_CSR_CMD_SIZE(len) | addr);
    expect_read32(ESC_CSR_CMD_REG, 0);
}

auto expect_read_csr(uint16_t addr, uint8_t *buf, size_t len) {
    uint32_t value = 0;
    memcpy((uint8_t *)&value, buf, len);
    expect_write32(ESC_CSR_CMD_REG,
                   ESC_CSR_CMD_READ | ESC_CSR_CMD_SIZE(len) | addr);
    expect_read32(ESC_CSR_CMD_REG, 0);
    expect_read32(ESC_CSR_DATA_REG, value);
}

auto expect_alevent_read() {
    uint8_t v[] = {0, 0};
    expect_read_csr(ESCREG_ALEVENT, v, sizeof(v));
}

auto expect_pram_write(uint16_t addr, uint16_t size) {
    expect_write32(ESC_PRAM_WR_CMD_REG, ESC_PRAM_CMD_ABORT);
    expect_read32(ESC_PRAM_WR_CMD_REG, 0);

    expect_write32(ESC_PRAM_WR_ADDR_LEN_REG,
                   ESC_PRAM_SIZE(size) | ESC_PRAM_ADDR(addr));
    expect_write32(ESC_PRAM_WR_CMD_REG, ESC_PRAM_CMD_BUSY);
    expect_read32(ESC_PRAM_WR_CMD_REG, 1);
}

auto expect_pram_read(uint16_t addr, uint16_t size) {
    expect_write32(ESC_PRAM_RD_CMD_REG, ESC_PRAM_CMD_ABORT);
    expect_read32(ESC_PRAM_RD_CMD_REG, 0);

    expect_write32(ESC_PRAM_RD_ADDR_LEN_REG,
                   ESC_PRAM_SIZE(size) | ESC_PRAM_ADDR(addr));
    expect_write32(ESC_PRAM_RD_CMD_REG, ESC_PRAM_CMD_BUSY);
    expect_read32(ESC_PRAM_RD_CMD_REG, 1);
}

// ----

TEST(ec_write_csr_4b_align) {
    uint8_t buf[] = {0x12, 0x34, 0x56, 0x78};

    expect_write_csr(0x10, buf, sizeof(buf));
    expect_alevent_read();

    ESC_write(0x10, buf, sizeof(buf));
}

TEST(ec_write_csr_2b_align) {
    uint8_t buf[] = {0x12, 0x34, 0x56, 0x78};

    expect_write_csr(0x12, buf, 2);
    expect_write_csr(0x14, buf + 2, 2);
    expect_alevent_read();

    ESC_write(0x12, buf, sizeof(buf));
}

TEST(ec_write_csr_1b_align) {
    uint8_t buf[] = {0x12, 0x34, 0x56, 0x78};

    expect_write_csr(0x13, buf, 1);
    expect_write_csr(0x14, buf + 1, 1);
    expect_write_csr(0x15, buf + 2, 1);
    expect_write_csr(0x16, buf + 3, 1);
    expect_alevent_read();

    ESC_write(0x13, buf, sizeof(buf));
}

TEST(ec_write_csr_1b_align2) {
    uint8_t buf[] = {0x12, 0x34, 0x56, 0x78};

    expect_write_csr(0x11, buf, 1);
    expect_write_csr(0x12, buf + 1, 1);
    expect_write_csr(0x13, buf + 2, 1);
    expect_write_csr(0x14, buf + 3, 1);
    expect_alevent_read();

    ESC_write(0x11, buf, sizeof(buf));
}

TEST(ec_read_csr_4b_align) {
    uint8_t rbuf[] = {0x12, 0x34, 0x56, 0x78};
    uint8_t buf[4] = {};

    expect_read_csr(0x10, rbuf, sizeof(rbuf));
    expect_alevent_read();

    ESC_read(0x10, buf, sizeof(buf));
    assert(memcmp(buf, rbuf, sizeof(rbuf)) == 0);
}

TEST(ec_read_csr_2b_align) {
    uint8_t rbuf[] = {0x12, 0x34, 0x56, 0x78};
    uint8_t buf[4] = {};

    expect_read_csr(0x12, rbuf, 2);
    expect_read_csr(0x14, rbuf + 2, 2);
    expect_alevent_read();

    ESC_read(0x12, buf, sizeof(buf));
    assert(memcmp(buf, rbuf, sizeof(rbuf)) == 0);
}


TEST(ec_write_pram_4b) {
    uint8_t buf[] = {0x12, 0x34, 0x56, 0x78};

    expect_pram_write(0x1000, 4);
    expect_writebuf(ESC_PRAM_WR_FIFO_REG, buf, sizeof(buf));
    expect_alevent_read();

    ESC_write(0x1000, buf, sizeof(buf));
}

TEST(ec_write_pram_9b) {
    uint8_t buf[] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0,
                     0xaa, 0, 0, 0};

    expect_pram_write(0x1000, 9);
    expect_writebuf(ESC_PRAM_WR_FIFO_REG, buf, 4);
    expect_writebuf(ESC_PRAM_WR_FIFO_REG, buf+4, 8);
    expect_alevent_read();

    ESC_write(0x1000, buf, 9);
}

TEST(ec_write_pram_16b) {
    uint8_t buf[] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0,
                     0x21, 0x43, 0x65, 0x87, 0xaa, 0xbb, 0xcc, 0xdd};

    expect_pram_write(0x1000, 16);
    expect_writebuf(ESC_PRAM_WR_FIFO_REG, buf, 4);
    expect_writebuf(ESC_PRAM_WR_FIFO_REG, buf+4, 12);
    expect_alevent_read();

    ESC_write(0x1000, buf, 16);
}

TEST(ec_write_pram_7b_unaligned) {
    uint8_t buf[] = {0,    0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde};

    expect_pram_write(0x1001, 7);
    expect_writebuf(ESC_PRAM_WR_FIFO_REG, buf, 4);
    expect_writebuf(ESC_PRAM_WR_FIFO_REG, buf+4, 4);
    expect_alevent_read();

    ESC_write(0x1001, buf+1, 7);
}

TEST(ec_read_pram_4b) {
    uint8_t rbuf[4] = {0x12, 0x34, 0x56, 0x78};
    uint8_t buf[4] = {0};

    expect_pram_read(0x1000, 4);
    expect_readbuf(ESC_PRAM_RD_FIFO_REG, rbuf, 4);
    expect_alevent_read();

    ESC_read(0x1000, buf, 4);
    assert(memcmp(buf, rbuf, 4) == 0);
}

TEST(ec_read_pram_9b) {
    uint8_t rbuf[] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0,
                      0xaa, 0, 0, 0};
    uint8_t buf[9] = {0};

    expect_pram_read(0x1000, 9);
    expect_readbuf(ESC_PRAM_RD_FIFO_REG, rbuf, 4);
    expect_readbuf(ESC_PRAM_RD_FIFO_REG, rbuf+4, 8);
    expect_alevent_read();

    ESC_read(0x1000, buf, 9);
    assert(memcmp(buf, rbuf, 9) == 0);
}
