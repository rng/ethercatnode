# STM32 + LAN9252 EtherCAT firmware

Using the [Simple Open EtherCAT Slave library](https://github.com/OpenEtherCATsociety/SOES)


## Requirements

- arm-none-eabi-gcc ([PPA](https://launchpad.net/~team-gcc-arm-embedded/+archive/ubuntu/ppa) for Ubuntu)
- openocd
- [SOEM](https://github.com/OpenEtherCATsociety/SOES) for `eepromtool`
- [siitool](https://github.com/synapticon/siitool) to generate EEPROM binary

## Building

    git submodule update
    make

