#pragma once

#include <stdlib.h>

#define ASSERT(expr) \
    if (!(expr)) {   \
        abort();     \
    }
