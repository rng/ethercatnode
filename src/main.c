#include <math.h>
#include <stdint.h>

#include "libopencm3/cm3/cortex.h"
#include "libopencm3/cm3/dwt.h"
#include "libopencm3/cm3/scb.h"
#include "libopencm3/stm32/adc.h"
#include "libopencm3/stm32/exti.h"
#include "libopencm3/stm32/timer.h"
#include "libopencmsis/core_cm3.h"

#include "a1335.h"
#include "board.h"
#include "build.h"
#include "common.h"
#include "drv8305.h"
#include "ethercat.h"
#include "flash.h"
#include "pwm.h"
#include "semaphore.h"
#include "ulog.h"

typedef struct {
    uint16_t count_in;
    uint16_t pad1;
    uint32_t pad2;
} rxpdo_t;
_Static_assert(sizeof(rxpdo_t) == 8, "RX PDO size not 64 bits");

typedef struct {
    uint16_t count_out;
    int16_t temp;
    uint16_t vref;
    uint16_t angle;
} txpdo_t;
_Static_assert(sizeof(txpdo_t) == 8, "TX PDO size not 64 bits");

typedef struct {
    bool reboot;
    uint8_t foe_buf[FLASH_PAGE_SIZE];
    rxpdo_t rxpdo;
    txpdo_t txpdo;
    sem_t esc_sem;
} main_priv_t;
main_priv_t main_priv;

static uint32_t foe_write(foe_writefile_cfg_t *self, uint8_t *data,
                          size_t length) {
    uint32_t addr    = self->dest_start_address + self->address_offset;
    uint16_t *data16 = (uint16_t *)data;

    if (flash_active_partition() == self->dest_start_address) {
        // Can't write to running partition
        return 1;
    }

    ulog_printf("flash write 0x%08x (%db)\n", addr, length);
    // align to half words
    if ((length < FLASH_PAGE_SIZE) && (length & 1)) {
        length++;
    }
    ASSERT((length % 2) == 0);
    ASSERT((addr % FLASH_PAGE_SIZE) == 0);

    flash_unlock();
    flash_erase_page(addr);
    for (size_t i = 0; i < length / 2; i++) {
        flash_program_half_word(addr + (i * 2), data16[i]);
    }
    flash_lock();

    return 0;
}

static void led_flash(uint32_t n, uint32_t ms) {
    for (uint32_t i = 0; i < n; i++) {
        board_set_led(true);
        msleep(ms);
        board_set_led(false);
        msleep(ms);
    }
}

static void ec_tick(void) {
    uint32_t cyc_start        = dwt_read_cycle_counter();
    main_priv.txpdo.count_out = main_priv.rxpdo.count_in;

    adc_start_conversion_regular(ADC1);
    while (!adc_eoc(ADC1)) {
    }
    main_priv.txpdo.temp = adc_to_temp_c(adc_read_regular(ADC1));
    while (!adc_eoc(ADC1)) {
    }
    main_priv.txpdo.vref = adc_to_vref_mv(adc_read_regular(ADC1));
    // a1335_result_t err;
    // main_priv.txpdo.angle = a1335_angle_read(&err);
    main_priv.txpdo.angle = dwt_read_cycle_counter() - cyc_start;
}

static void ec_stop(void) {
    ulog_printf("[EC] stop\n");
}

static void ec_state_change(uint8_t from, uint8_t to) {
    static const char *states[] = {"?", "init", "preop", "boot", "safeop",
                                   "?", "?",    "?",     "op"};
    (void)from;
    ulog_printf("[EC] state: %s\n", states[to]);
    switch (to) {
        case ETHERCAT_STATE_INIT:
        case ETHERCAT_STATE_BOOT:
        case ETHERCAT_STATE_PREOP:
        case ETHERCAT_STATE_SAFEOP:
        case ETHERCAT_STATE_OP:
            break;
        default:
            abort();
    }
    if ((from == ETHERCAT_STATE_BOOT) && (to == ETHERCAT_STATE_INIT)) {
        main_priv.reboot = true;
    }
}

void exti9_5_isr(void) {
    exti_reset_request(EXTI6);
    sem_post(&main_priv.esc_sem);
}

uint8_t pwm = 0;

void tim2_isr(void) {

    if (timer_get_flag(TIM2, TIM_SR_CC1IF)) {
        timer_clear_flag(TIM2, TIM_SR_CC1IF);

        pwm++;
        float va      = sinf((M_PI * 2 * (float)(pwm + 0)) / 255.);
        float vb      = sinf((M_PI * 2 * (float)((pwm + 85) % 256)) / 255.);
        float vc      = sinf((M_PI * 2 * (float)((pwm + 170) % 256)) / 255.);
        float pwmhmax = 20.f;
        int pwma      = va * pwmhmax + pwmhmax;
        int pwmb      = vb * pwmhmax + pwmhmax;
        int pwmc      = vc * pwmhmax + pwmhmax;
        pwm_set(pwma, pwmb, pwmc);
    }
}

static foe_writefile_cfg_t foe_files[] = {
    {
        .name               = "firmware-a.bin",
        .max_data           = PARTITION_SIZE,
        .dest_start_address = A_PARTITION,
        .address_offset     = 0,
        .filepass           = 0,
        .write_function     = foe_write,
    },
    {
        .name               = "firmware-b.bin",
        .max_data           = PARTITION_SIZE,
        .dest_start_address = B_PARTITION,
        .address_offset     = 0,
        .filepass           = 0,
        .write_function     = foe_write,
    },
};

static const foe_cfg_t foe_cfg = {
    .fbuffer     = main_priv.foe_buf,
    .buffer_size = FLASH_PAGE_SIZE,
    .n_files     = sizeof(foe_files) / sizeof(foe_files[0]),
    .files       = foe_files,
};

int main() {
    ethercat_cfg_t ec_cfg = {
        .foe          = (foe_cfg_t *)&foe_cfg,
        .rxpdo        = &main_priv.rxpdo,
        .rxpdo_len    = sizeof(main_priv.rxpdo),
        .txpdo        = &main_priv.txpdo,
        .txpdo_len    = sizeof(main_priv.txpdo),
        .tick         = ec_tick,
        .stop         = ec_stop,
        .state_change = ec_state_change,
        .fw_version   = app_header.buildversion,
    };
    const char partition = flash_active_partition() == A_PARTITION ? 'A' : 'B';

    cm_enable_interrupts();
    dwt_enable_cycle_counter();
    board_init();
    pwm_init();
    ulog_init();

    ulog_printf("\nBooted %c: v%d (%s)\n", partition, app_header.buildversion,
                app_header.buildsha);

    ethercat_init(&ec_cfg);
    // a1335_clear_error();
    drv8305_err_t err;
    drv8305_init(&err);
    drv8305_enable(true);
    drv8305_reg_dump();

    led_flash(4, 75);
    ulog_printf("running\n");

    while (1) {
        sem_wait(&main_priv.esc_sem);
        ethercat_tick();

        if (drv8305_is_fault()) {
            ulog_printf("motor fault\n");
            drv8305_reg_dump();
        }

        if (main_priv.reboot) {
            ulog_printf("rebooting to bootloader\n");
            msleep(1);
            cm_disable_interrupts();
            scb_reset_system();
        }
    }

    return 0;
}
