#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "soes/esc.h"

#define BIT(x) 1 << (x)

typedef enum {
    ESC_CMD_SERIAL_WRITE = 0x02,
    ESC_CMD_SERIAL_READ  = 0x03,
    ESC_CMD_FAST_READ    = 0x0B,
} ethercat_spi_cmd_t;

#define ESC_CMD_FAST_READ_DUMMY 1
#define ESC_CMD_ADDR_INC BIT(6)

// LAN9252 (non CSR) registers
typedef enum {
    ESC_PRAM_RD_FIFO_REG     = 0x000,
    ESC_PRAM_WR_FIFO_REG     = 0x020,
    ESC_ID_REV_REG           = 0x050,
    ESC_IRQ_CFG_REG          = 0x054,
    ESC_INT_STS_REG          = 0x058,
    ESC_INT_EN_REG           = 0x05C,
    ESC_BYTE_TEST_REG        = 0x064,
    ESC_RESET_CTRL_REG       = 0x1F8,
    ESC_CSR_DATA_REG         = 0x300,
    ESC_CSR_CMD_REG          = 0x304,
    ESC_PRAM_RD_ADDR_LEN_REG = 0x308,
    ESC_PRAM_RD_CMD_REG      = 0x30C,
    ESC_PRAM_WR_ADDR_LEN_REG = 0x310,
    ESC_PRAM_WR_CMD_REG      = 0x314,
} ethercat_reg_t;

#define ESC_IRQ_CFG_IRQ_EN BIT(8)
#define ESC_INT_READY BIT(30)
#define ESC_INT_ECAT BIT(0)

// CSR registers (see also lib/SOES/soes/esc.h)
typedef enum {
    ESCREG_TYPE             = 0x000,
    ESCREG_PRODUCT_ID       = 0xE00,
    ESCREG_VENDOR_ID        = 0xE08,
    ESCREG_PHY_PORT0_STATUS = 0x518,
} ethercat_csr_reg_t;

#define ESC_PRAM_CMD_BUSY BIT(31)
#define ESC_PRAM_CMD_ABORT BIT(30)

#define ESC_PRAM_CMD_CNT(x) ((x >> 8) & 0x1F)
#define ESC_PRAM_CMD_AVAIL BIT(0)

#define ESC_PRAM_SIZE(x) ((x) << 16)
#define ESC_PRAM_ADDR(x) ((x) << 0)

#define ESC_CSR_CMD_BUSY BIT(31)
#define ESC_CSR_CMD_READ (BIT(31) | BIT(30))
#define ESC_CSR_CMD_WRITE BIT(31)
#define ESC_CSR_CMD_SIZE(x) (x << 16)

#define ESC_RESET_CTRL_ECAT_RST BIT(6)
#define ESC_RESET_CTRL_PHYB_RST BIT(2)
#define ESC_RESET_CTRL_PHYA_RST BIT(1)
#define ESC_RESET_CTRL_DIGI_RST BIT(0)

uint32_t ESC_int_clear(void);
// prototypes declared in esc.h
