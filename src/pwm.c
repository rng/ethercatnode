#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include "board.h"
#include "common.h"

static void pwm_init_channel(enum tim_oc_id chan, enum tim_oc_id chann) {
    timer_set_oc_value(TIM1, chan, 0);           // sets TIMx_CCRx
    timer_set_oc_mode(TIM1, chan, TIM_OCM_PWM1); // Sets PWM Mode 1

    timer_set_oc_polarity_high(TIM1, chan);
    timer_set_oc_polarity_high(TIM1, chann);
    timer_enable_oc_output(TIM1, chan);
    timer_enable_oc_output(TIM1, chann);
}

void pwm_init(void) {
    uint32_t period = 256;
    rcc_periph_reset_pulse(RST_TIM1);
    timer_set_mode(TIM1, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
    timer_continuous_mode(TIM1);
    timer_set_period(TIM1, period);

    timer_enable_break_main_output(TIM1);

    timer_set_prescaler(TIM1, 1);
    timer_set_clock_division(TIM1, 0);
    timer_set_master_mode(TIM1, TIM_CR2_MMS_UPDATE);
    timer_disable_preload(TIM1);
    timer_continuous_mode(TIM1);

    timer_enable_counter(TIM1);

    pwm_init_channel(TIM_OC1, TIM_OC1N);
    pwm_init_channel(TIM_OC2, TIM_OC2N);
    pwm_init_channel(TIM_OC3, TIM_OC3N);
}

void pwm_set(int32_t a, int32_t b, int32_t c) {
    if (a == -1) {
        timer_disable_oc_output(TIM1, TIM_OC1);
        timer_disable_oc_output(TIM1, TIM_OC1N);
    } else {
        timer_enable_oc_output(TIM1, TIM_OC1);
        timer_enable_oc_output(TIM1, TIM_OC1N);
        ASSERT((a >= 0) && (a <= 255));
        timer_set_oc_value(TIM1, TIM_OC1, a);
    }

    if (b == -1) {
        timer_disable_oc_output(TIM1, TIM_OC2);
        timer_disable_oc_output(TIM1, TIM_OC2N);
    } else {
        timer_enable_oc_output(TIM1, TIM_OC2);
        timer_enable_oc_output(TIM1, TIM_OC2N);
        ASSERT((b >= 0) && (b <= 255));
        timer_set_oc_value(TIM1, TIM_OC2, b);
    }

    if (c == -1) {
        timer_disable_oc_output(TIM1, TIM_OC3);
        timer_disable_oc_output(TIM1, TIM_OC3N);
    } else {
        timer_enable_oc_output(TIM1, TIM_OC3);
        timer_enable_oc_output(TIM1, TIM_OC3N);
        ASSERT((c >= 0) && (c <= 255));
        timer_set_oc_value(TIM1, TIM_OC3, c);
    }
}
