#include <stdint.h>

#include "libopencm3/cm3/cortex.h"
#include "libopencm3/cm3/scb.h"
#include "libopencm3/cm3/vector.h"
#include "libopencm3/stm32/rcc.h"

#include "app_header.h"
#include "common.h"
#include "flash.h"

void abort(void) {
    while (1) {
    }
}

static const struct rcc_clock_scale _rcc_hsi_8mhz = {
    .pllsrc           = RCC_CFGR_PLLSRC_HSI_DIV2,
    .pllmul           = RCC_CFGR_PLLMUL_MUL16,
    .plldiv           = RCC_CFGR2_PREDIV_NODIV,
    .hpre             = RCC_CFGR_HPRE_DIV_NONE,
    .ppre1            = RCC_CFGR_PPRE1_DIV_2,
    .ppre2            = RCC_CFGR_PPRE2_DIV_NONE,
    .flash_waitstates = 2,
    .ahb_frequency    = 64000000,
    .apb1_frequency   = 32000000,
    .apb2_frequency   = 64000000,
};

static void clock_init(void) {
    rcc_clock_setup_hsi(&_rcc_hsi_8mhz);
}

static void clock_deinit(void) {
    /* Enable internal high-speed oscillator. */
    rcc_osc_on(RCC_HSI);
    rcc_wait_for_osc_ready(RCC_HSI);

    /* Reset the RCC_CFGR register */
    RCC_CFGR = 0x000000;

    rcc_osc_off(RCC_HSE);
    rcc_osc_off(RCC_PLL);

    /* Reset the HSEBYP bit */
    rcc_osc_bypass_disable(RCC_HSE);

    /* Reset the CIR register */
    RCC_CIR = 0x000000;
}

static void __attribute__((noreturn)) jump_to(uint32_t addr) {
    const uint32_t *app_image = (const uint32_t *)addr;
    uint32_t stacktop         = app_image[0];
    uint32_t entrypoint       = app_image[1];

    cm_disable_interrupts();
    clock_deinit();

    SCB_VTOR = addr;

    asm volatile("msr msp, %0\n"
                 "bx %1\n"
                 :
                 : "r"(stacktop), "r"(entrypoint)
                 :);

    while (1) {
    }
}

static const app_header_t *partition_header(uint32_t addr) {
    return (app_header_t *)(addr + sizeof(vector_table_t));
}

static bool partition_is_valid(uint32_t addr) {
    const app_header_t *hdr = partition_header(addr);
    return hdr->magic == APP_MAGIC_ID;
}

static bool force_old_partition(void) {
    return false; // FIXME: use GPIO to toggle this
}

static uint32_t latest_partition(void) {
    if (partition_is_valid(A_PARTITION) && partition_is_valid(B_PARTITION)) {
        const app_header_t *a = partition_header(A_PARTITION);
        const app_header_t *b = partition_header(B_PARTITION);

        uint32_t new, old;
        if (a->buildtime >= b->buildtime) {
            new = A_PARTITION;
            old = B_PARTITION;
        } else {
            new = B_PARTITION;
            old = A_PARTITION;
        }

        return force_old_partition() ? old : new;
    } else if (partition_is_valid(A_PARTITION)) {
        return A_PARTITION;
    } else if (partition_is_valid(B_PARTITION)) {
        return B_PARTITION;
    }
    abort();
}

int main() {
    clock_init();
    jump_to(latest_partition());
    return 0;
}
