#pragma once

#include <stdatomic.h>

typedef struct {
    atomic_int v;
} sem_t;

void sem_wait(sem_t *s) {
    while (1) {
        while (atomic_load(&(s->v)) < 1) {
            __WFI();
        }
        // try to take the lock.
        int tmp = atomic_fetch_add(&(s->v), -1);
        if (tmp >= 1) {
            // we successfully got the lock
            break;
        } else {
            // undo our attempt; another thread's decrement happened first
            atomic_fetch_add(&(s->v), 1);
        }
    }
}

void sem_post(sem_t *s) {
    atomic_fetch_add(&(s->v), 1);
}
