#include "ethercat_hw.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "board.h"
#include "common.h"
#include "ulog.h"

static uint8_t lan9252_tmpbuf[256];

static void lan9252_write(uint16_t address, uint8_t *buf, size_t buf_len) {
    uint8_t cmd[3];

    cmd[0] = ESC_CMD_SERIAL_WRITE;
    cmd[1] = ((address >> 8) & 0xFF);
    cmd[2] = (address & 0xFF);

    gpio_clear(GPIOB, GPIO12);
    for (uint32_t i = 0; i < sizeof(cmd); i++) {
        spi_send8(ECAT_SPI, cmd[i]);
        spi_read8(ECAT_SPI);
    }
    for (uint32_t i = 0; i < buf_len; i++) {
        spi_send8(ECAT_SPI, buf[i]);
        spi_read8(ECAT_SPI);
    }
    gpio_set(GPIOB, GPIO12);
}

static void lan9252_write_32(uint16_t address, uint32_t val) {
    uint8_t data[4];

    data[0] = val & 0xFF;
    data[1] = (val >> 8) & 0xFF;
    data[2] = (val >> 16) & 0xFF;
    data[3] = (val >> 24) & 0xFF;
    lan9252_write(address, data, sizeof(data));
}

static void lan9252_read(uint32_t address, uint8_t *buf, size_t buf_len) {
    uint8_t cmd[3];

    cmd[0] = ESC_CMD_SERIAL_READ;
    cmd[1] = ((address >> 8) & 0xFF);
    cmd[2] = (address & 0xFF);

    gpio_clear(GPIOB, GPIO12);
    for (uint32_t i = 0; i < sizeof(cmd); i++) {
        spi_send8(ECAT_SPI, cmd[i]);
        spi_read8(ECAT_SPI);
    }
    for (uint32_t i = 0; i < buf_len; i++) {
        spi_send8(ECAT_SPI, 0);
        buf[i] = spi_read8(ECAT_SPI);
    }
    gpio_set(GPIOB, GPIO12);
}

static uint32_t lan9252_read_32(uint32_t address) {
    uint8_t data[4] = {0};

    lan9252_read(address, data, sizeof(data));
    return ((data[3] << 24) | (data[2] << 16) | (data[1] << 8) | data[0]);
}

#define LAN9252_CMD_WAIT(reg, busybit, value) \
    {                                         \
        lan9252_write_32(reg, value);         \
        uint32_t cmd_val;                     \
        do {                                  \
            cmd_val = lan9252_read_32(reg);   \
        } while (cmd_val & busybit);          \
    }

static void ESC_read_csr(uint16_t address, void *buf, uint16_t len) {
    ASSERT(len <= 4);

    uint32_t value = (ESC_CSR_CMD_READ | ESC_CSR_CMD_SIZE(len) | address);
    LAN9252_CMD_WAIT(ESC_CSR_CMD_REG, ESC_CSR_CMD_BUSY, value);

    value = lan9252_read_32(ESC_CSR_DATA_REG);
    memcpy(buf, (uint8_t *)&value, len);
}
/*
static uint16_t ESC_read_csr_16(uint16_t address) {
    uint16_t value = 0;
    ESC_read_csr(address, &value, sizeof(value));
    return value;
}

static uint32_t ESC_read_csr_32(uint16_t address) {
    uint32_t value = 0;
    ESC_read_csr(address, &value, sizeof(value));
    return value;
}
*/

static void ESC_write_csr(uint16_t address, void *buf, uint16_t len) {
    ASSERT(len <= 4);

    uint32_t value = 0;
    memcpy((uint8_t *)&value, buf, len);
    lan9252_write_32(ESC_CSR_DATA_REG, value);
    value = (ESC_CSR_CMD_WRITE | ESC_CSR_CMD_SIZE(len) | address);
    LAN9252_CMD_WAIT(ESC_CSR_CMD_REG, ESC_CSR_CMD_BUSY, value);
}

/* ESC read process data ram function */
static void ESC_read_pram(uint16_t address, void *buf, uint16_t len) {
    uint32_t value;
    uint8_t *temp_buf = buf;

    value = ESC_PRAM_CMD_ABORT;
    LAN9252_CMD_WAIT(ESC_PRAM_RD_CMD_REG, ESC_PRAM_CMD_BUSY, value);

    value = ESC_PRAM_SIZE(len) | ESC_PRAM_ADDR(address);
    lan9252_write_32(ESC_PRAM_RD_ADDR_LEN_REG, value);

    value = ESC_PRAM_CMD_BUSY;
    lan9252_write_32(ESC_PRAM_RD_CMD_REG, value);

    do {
        value = lan9252_read_32(ESC_PRAM_RD_CMD_REG);
    } while ((value & ESC_PRAM_CMD_AVAIL) == 0);

    // Read first value from FIFO
    value = lan9252_read_32(ESC_PRAM_RD_FIFO_REG);

    // Find out first byte position and adjust the copy from that
    // according to LAN9252 datasheet and MicroChip SDK code
    uint8_t first_byte_pos = (address & 0x03);
    uint8_t temp_len =
        ((4 - first_byte_pos) > len) ? len : (4 - first_byte_pos);

    memcpy(temp_buf, ((uint8_t *)&value + first_byte_pos), temp_len);
    len -= temp_len;
    uint16_t byte_offset = temp_len;

    if (len > 0) {
        // align size to word boundaries
        size_t size = (len + 3) & ~3;
        ASSERT(size <= sizeof(lan9252_tmpbuf));
        uint8_t *buffer = lan9252_tmpbuf;

        lan9252_read(ESC_PRAM_RD_FIFO_REG, buffer, size);

        memcpy(temp_buf + byte_offset, buffer, len);
    }
}

/* ESC write process data ram function */
static void ESC_write_pram(uint16_t address, void *buf, uint16_t len) {
    uint32_t value;
    uint8_t *temp_buf = buf;

    value = ESC_PRAM_CMD_ABORT;
    LAN9252_CMD_WAIT(ESC_PRAM_WR_CMD_REG, ESC_PRAM_CMD_BUSY, value);

    value = ESC_PRAM_SIZE(len) | ESC_PRAM_ADDR(address);
    lan9252_write_32(ESC_PRAM_WR_ADDR_LEN_REG, value);

    value = ESC_PRAM_CMD_BUSY;
    lan9252_write_32(ESC_PRAM_WR_CMD_REG, value);

    do {
        value = lan9252_read_32(ESC_PRAM_WR_CMD_REG);
    } while ((value & ESC_PRAM_CMD_AVAIL) == 0);

    // Find out first byte position and adjust the copy from that
    // according to LAN9252 datasheet
    uint8_t first_byte_pos = (address & 0x03);
    uint8_t temp_len =
        ((4 - first_byte_pos) > len) ? len : (4 - first_byte_pos);

    value = 0;
    memcpy(((uint8_t *)&value + first_byte_pos), temp_buf, temp_len);

    // Write first value from FIFO
    lan9252_write_32(ESC_PRAM_WR_FIFO_REG, value);

    len -= temp_len;
    uint16_t byte_offset = temp_len;

    if (len > 0) {
        // align size to word boundaries
        size_t size = (len + 3) & ~3;
        ASSERT(size <= sizeof(lan9252_tmpbuf));
        uint8_t *buffer = lan9252_tmpbuf;
        memset(buffer, 0, size);
        memcpy(buffer, temp_buf + byte_offset, len);

        lan9252_write(ESC_PRAM_WR_FIFO_REG, buffer, size);
    }
}

static uint16_t lan9252_size_align(uint16_t address, uint16_t size) {
    // Align size to address according to LAN9252 datasheet Table 12-14:
    // size | addr & 3
    // 1    | 0,1,2,3
    // 2    | 0,2
    // 4    | 0
    if (address & BIT(0)) {
        // if address is odd
        return 1;
    } else if (address & BIT(1)) {
        // If address 1xb and size != 1 and 3 , allow size 2 else size 1
        return (size & BIT(0)) ? 1 : 2;
    } else if (size == 3) {
        // size 3 not valid
        return 1;
    } else {
        // else size is kept as is
        return size;
    }
}

// Public API:

void ESC_read(uint16_t address, void *buf, uint16_t len) {
    // Select Read function depending on address, process data ram or not
    if (address >= 0x1000) {
        ESC_read_pram(address, buf, len);
    } else {
        uint8_t *temp_buf = (uint8_t *)buf;

        while (len > 0) {
            uint16_t size = lan9252_size_align(address, (len > 4) ? 4 : len);
            ESC_read_csr(address, temp_buf, size);
            /* next address */
            len -= size;
            temp_buf += size;
            address += size;
        }
    }
    // To mimic the ET1100 always providing AlEvent on every read or write
    ESC_read_csr(ESCREG_ALEVENT, (void *)&ESCvar.ALevent,
                 sizeof(ESCvar.ALevent));
}

void ESC_write(uint16_t address, void *buf, uint16_t len) {
    // Select Write function depending on address, process data ram or not
    if (address >= 0x1000) {
        ESC_write_pram(address, buf, len);
    } else {
        uint8_t *temp_buf = (uint8_t *)buf;

        while (len > 0) {
            uint16_t size = lan9252_size_align(address, (len > 4) ? 4 : len);
            ESC_write_csr(address, temp_buf, size);
            /* next address */
            len -= size;
            temp_buf += size;
            address += size;
        }
    }

    // To mimic the ET1x00 always providing AlEvent on every read or write
    ESC_read_csr(ESCREG_ALEVENT, (void *)&ESCvar.ALevent,
                 sizeof(ESCvar.ALevent));
}

uint32_t ESC_int_clear(void) {
    uint32_t value = lan9252_read_32(ESC_INT_STS_REG);
    lan9252_write_32(ESC_INT_STS_REG, value);
    return value;
}

void ESC_init(const esc_cfg_t *config) {
    (void)config;
    uint32_t value;

    msleep(10);
    lan9252_write_32(ESC_RESET_CTRL_REG, ESC_RESET_CTRL_DIGI_RST);
    do {
        value = lan9252_read_32(ESC_RESET_CTRL_REG);
        msleep(10);
    } while (value & ESC_RESET_CTRL_DIGI_RST);
    lan9252_write_32(ESC_RESET_CTRL_REG,
                     ESC_RESET_CTRL_PHYA_RST | ESC_RESET_CTRL_PHYB_RST);
    msleep(5);

    value = lan9252_read_32(ESC_BYTE_TEST_REG);
    if (value != 0x87654321) {
        ulog_printf("ESC byte test failed %x\n", value);
        abort();
    }

    value = lan9252_read_32(ESC_INT_STS_REG);
    lan9252_write_32(ESC_INT_STS_REG, value);
    ulog_printf("[EC] hardware init ok\n");
    // ulog_printf("Revision    %x\n", ESC_read_csr_32(ESCREG_TYPE));
    // ulog_printf("Product     %x\n", ESC_read_csr_32(ESCREG_PRODUCT_ID));
    // ulog_printf("Vendor      %x\n", ESC_read_csr_32(ESCREG_VENDOR_ID));
    // ulog_printf("PHY0 status %x\n",
    // ESC_read_csr_32(ESCREG_PHY_PORT0_STATUS));

    // enable ECAT interrupt, then enable master interrupts
    lan9252_write_32(ESC_INT_EN_REG, ESC_INT_ECAT | ESC_INT_READY);
    lan9252_write_32(ESC_IRQ_CFG_REG, ESC_IRQ_CFG_IRQ_EN);
}
