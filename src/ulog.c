#include "ulog.h"
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include "common.h"
#include "tinyprintf.h"

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/usart.h>

#ifndef USE_ULOG

void ulog_init(void) {
}

void ulog_print(char *data) {
    (void)data;
}

void ulog_printf(const char *format, ...) {
    (void)format;
}

#else

typedef struct {
    volatile bool busy;
    char buf[128];
} ulog_priv_t;
ulog_priv_t ulog_priv;

static void clock_setup(void) {
    rcc_periph_clock_enable(RCC_USART3);
    rcc_periph_clock_enable(RCC_DMA1);
}

static void usart_setup(uint32_t baudrate) {
    usart_set_baudrate(USART3, baudrate);
    usart_set_databits(USART3, 8);
    usart_set_stopbits(USART3, USART_STOPBITS_1);
    usart_set_mode(USART3, USART_MODE_TX);
    usart_set_parity(USART3, USART_PARITY_NONE);
    usart_set_flow_control(USART3, USART_FLOWCONTROL_NONE);

    usart_enable(USART3);

    nvic_set_priority(NVIC_DMA1_CHANNEL2_IRQ, 0);
    nvic_enable_irq(NVIC_DMA1_CHANNEL2_IRQ);
}

void dma1_channel2_isr(void) {
    if ((DMA1_ISR & DMA_ISR_TCIF2) != 0) {
        DMA1_IFCR |= DMA_IFCR_CTCIF2;
    }

    dma_disable_transfer_complete_interrupt(DMA1, DMA_CHANNEL2);
    usart_disable_tx_dma(USART3);
    dma_disable_channel(DMA1, DMA_CHANNEL2);
    ulog_priv.busy = false;
}

static void ulog_wait(void) {
    while (ulog_priv.busy)
        ;
    ulog_priv.busy = true;
}

static void ulog_start_tx(int size) {
    dma_channel_reset(DMA1, DMA_CHANNEL2);

    dma_set_peripheral_address(DMA1, DMA_CHANNEL2, (uint32_t)&USART3_TDR);
    dma_set_memory_address(DMA1, DMA_CHANNEL2, (uint32_t)ulog_priv.buf);
    dma_set_number_of_data(DMA1, DMA_CHANNEL2, size);
    dma_set_read_from_memory(DMA1, DMA_CHANNEL2);
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL2);
    dma_set_peripheral_size(DMA1, DMA_CHANNEL2, DMA_CCR_PSIZE_8BIT);
    dma_set_memory_size(DMA1, DMA_CHANNEL2, DMA_CCR_MSIZE_8BIT);
    dma_set_priority(DMA1, DMA_CHANNEL2, DMA_CCR_PL_VERY_HIGH);

    dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL2);
    dma_enable_channel(DMA1, DMA_CHANNEL2);

    usart_enable_tx_dma(USART3);
}

void ulog_printf(const char *format, ...) {
    va_list ap;

    ulog_wait();
    va_start(ap, format);
    int size = tfp_vsnprintf(ulog_priv.buf, sizeof(ulog_priv.buf), format, ap);
    va_end(ap);
    ulog_start_tx(size);
}

void ulog_init(void) {
    clock_setup();
    usart_setup(460800);
}

#endif
