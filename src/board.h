#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "libopencm3/stm32/gpio.h"
#include "libopencm3/stm32/spi.h"

#define ECAT_SPI SPI2
#define DRV_SPI SPI3

void board_init(void);
void board_set_led(bool on);

void msleep(uint32_t delay);
uint32_t adc_to_vref_mv(uint32_t adc);
uint32_t adc_to_temp_c(uint32_t adc);
