#pragma once

#include "soes/esc_foe.h"

typedef enum {
    ETHERCAT_STATE_INIT   = 1,
    ETHERCAT_STATE_PREOP  = 2,
    ETHERCAT_STATE_BOOT   = 3,
    ETHERCAT_STATE_SAFEOP = 4,
    ETHERCAT_STATE_OP     = 8,
} ethercat_state_t;

typedef void (*ethercat_tick_cb_t)(void);
typedef void (*ethercat_stop_cb_t)(void);
typedef void (*ethercat_statechange_cb_t)(ethercat_state_t from,
                                          ethercat_state_t to);

typedef struct {
    //
    foe_cfg_t *foe;
    ethercat_tick_cb_t tick;
    ethercat_stop_cb_t stop;
    ethercat_statechange_cb_t state_change;

    //
    void *rxpdo;
    void *txpdo;
    size_t rxpdo_len;
    size_t txpdo_len;

    //
    uint32_t fw_version;
} ethercat_cfg_t;

void ethercat_init(ethercat_cfg_t *cfg);
void ethercat_tick(void);
