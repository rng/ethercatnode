#pragma once

#include <stdint.h>

void pwm_init(void);
void pwm_set(int32_t a, int32_t b, int32_t c);
