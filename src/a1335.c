#include "a1335.h"
#include "libopencm3/stm32/i2c.h"

#define A1335_ADDR 0x18
#define A1335_ANG_PARITY_OFS 12
#define A1335_ANG_NEW_OFS 13
#define A1335_ANG_ERR_OFS 14

#define BYTESWAP_U16(val) (((val & 0xff) << 8) | ((val >> 8) & 0xff))

uint16_t a1335_reg_read16(a1335_reg_t reg, a1335_result_t *err) {
    uint8_t cmd[1] = {reg};
    uint16_t resp  = 0;

    *err = A1335_STATUS_OK;
    i2c_transfer7(I2C1, A1335_ADDR, cmd, sizeof(cmd), (uint8_t *)&resp,
                  sizeof(resp));
    return BYTESWAP_U16(resp);
}

void a1335_reg_write16(a1335_reg_t reg, uint16_t value, a1335_result_t *err) {
    uint8_t cmd[3] = {reg, (uint8_t)(value >> 8), (uint8_t)(value & 0xff)};

    *err = A1335_STATUS_OK;
    i2c_transfer7(I2C1, A1335_ADDR, cmd, sizeof(cmd), NULL, 0);
}

void a1335_clear_error(void) {
    a1335_result_t err;
    a1335_reg_read16(A1335_REG_XERR, &err);
    a1335_reg_read16(A1335_REG_ERR, &err);
    a1335_reg_write16(A1335_REG_CTRL, 0x0346, &err);
}

uint16_t a1335_angle_read(a1335_result_t *err) {
    *err         = A1335_STATUS_OK;
    uint16_t raw = a1335_reg_read16(A1335_REG_ANG, err);
    if (raw & (1 << A1335_ANG_ERR_OFS)) {
        *err = A1335_STATUS_ERR_GENERIC;
    }
    return raw & 0x0FFF;
}

int16_t a1335_temp_read(a1335_result_t *err) {
    uint32_t raw = a1335_reg_read16(A1335_REG_TSEN, err) & 0x0FFF;
    int32_t tmp  = (raw / 8) - 273;
    return (int16_t)tmp;
}

uint16_t a1335_field_read(a1335_result_t *err) {
    return a1335_reg_read16(A1335_REG_FIELD, err) & 0x0FFF;
}
