#include <stdint.h>

#include "board.h"
#include "libopencm3/cm3/nvic.h"
#include "libopencm3/cm3/systick.h"
#include "libopencm3/stm32/adc.h"
#include "libopencm3/stm32/exti.h"
#include "libopencm3/stm32/gpio.h"
#include "libopencm3/stm32/i2c.h"
#include "libopencm3/stm32/rcc.h"
#include "libopencm3/stm32/spi.h"
#include "libopencm3/stm32/timer.h"
#include "libopencm3/stm32/usart.h"

#define GPIO_INP(PORT, PINS, PULL) \
    gpio_mode_setup(GPIO##PORT, GPIO_MODE_INPUT, GPIO_PUPD_##PULL, PINS)
#define GPIO_OUT(PORT, PINS, PULL) \
    gpio_mode_setup(GPIO##PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_##PULL, PINS)
#define GPIO_ALT(PORT, PINS, PULL, AFNUM)                              \
    gpio_mode_setup(GPIO##PORT, GPIO_MODE_AF, GPIO_PUPD_##PULL, PINS); \
    gpio_set_af(GPIO##PORT, GPIO_##AFNUM, PINS)
#define GPIO_OPT(PORT, PINS, DRV, SPD)                                       \
    gpio_set_output_options(GPIO##PORT, GPIO_OTYPE_##DRV, GPIO_OSPEED_##SPD, \
                            PINS)

const struct rcc_clock_scale rcc_clk_config = {
    .pllsrc           = RCC_CFGR_PLLSRC_HSE_PREDIV,
    .pllmul           = RCC_CFGR_PLLMUL_MUL4,
    .plldiv           = RCC_CFGR2_PREDIV_NODIV,
    .usbdiv1          = false,
    .flash_waitstates = 2,
    .hpre             = RCC_CFGR_HPRE_DIV_NONE,
    .ppre1            = RCC_CFGR_PPRE1_DIV_2,
    .ppre2            = RCC_CFGR_PPRE2_DIV_NONE,
    .ahb_frequency    = 64e6,
    .apb1_frequency   = 32e6,
    .apb2_frequency   = 64e6,
};

void __attribute__((noinline)) abort() {
    while (1) {
        for (int i = 0; i < 2; i++) {
            board_set_led(true);
            msleep(100);
            board_set_led(false);
            msleep(100);
        }
        msleep(1000);
    }
}

static void clock_init(void) {
    rcc_osc_on(RCC_HSE);
    rcc_wait_for_osc_ready(RCC_HSE);
    rcc_set_sysclk_source(RCC_HSE);

    rcc_clock_setup_pll(&rcc_clk_config);

    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_GPIOD);
    rcc_periph_clock_enable(RCC_SPI2);
    rcc_periph_clock_enable(RCC_SPI3);
    // rcc_periph_clock_enable(RCC_I2C1);
    rcc_periph_clock_enable(RCC_TIM1);
    rcc_periph_clock_enable(RCC_TIM2);
    rcc_periph_clock_enable(RCC_ADC12);
    rcc_periph_clock_enable(RCC_SYSCFG);
}

static void spi_init(void) {
    // ECAT
    spi_reset(ECAT_SPI);
    spi_set_master_mode(ECAT_SPI);
    spi_set_baudrate_prescaler(ECAT_SPI, SPI_CR1_BR_FPCLK_DIV_16);
    spi_set_clock_polarity_0(ECAT_SPI);
    spi_set_clock_phase_0(ECAT_SPI);
    spi_set_full_duplex_mode(ECAT_SPI);
    spi_set_data_size(ECAT_SPI, SPI_CR2_DS_8BIT);

    spi_enable_software_slave_management(ECAT_SPI);
    spi_set_nss_high(ECAT_SPI);

    spi_enable_ss_output(ECAT_SPI);
    spi_send_msb_first(ECAT_SPI);
    spi_fifo_reception_threshold_8bit(ECAT_SPI);
    SPI_I2SCFGR(ECAT_SPI) &= ~SPI_I2SCFGR_I2SMOD;

    spi_enable(ECAT_SPI);

    // DRV
    spi_reset(DRV_SPI);
    spi_set_master_mode(DRV_SPI);
    spi_set_baudrate_prescaler(DRV_SPI, SPI_CR1_BR_FPCLK_DIV_16);
    spi_set_clock_polarity_0(DRV_SPI);
    spi_set_clock_phase_1(DRV_SPI);
    spi_set_full_duplex_mode(DRV_SPI);
    spi_set_data_size(DRV_SPI, SPI_CR2_DS_8BIT);

    spi_enable_software_slave_management(DRV_SPI);
    spi_set_nss_high(DRV_SPI);

    spi_enable_ss_output(DRV_SPI);
    spi_send_msb_first(DRV_SPI);
    spi_fifo_reception_threshold_8bit(DRV_SPI);
    SPI_I2SCFGR(DRV_SPI) &= ~SPI_I2SCFGR_I2SMOD;

    spi_enable(DRV_SPI);
}

/*
static void i2c_init(void) {
    rcc_set_i2c_clock_sysclk(I2C1);
    i2c_reset(I2C1);
    GPIO_ALT(B, GPIO6 | GPIO7, PULLUP, AF4);
    i2c_peripheral_disable(I2C1);
    i2c_enable_analog_filter(I2C1);
    i2c_set_digital_filter(I2C1, 0);
    i2c_set_speed(I2C1, i2c_speed_sm_100k, 64); // 64Mhz clock
    i2c_enable_stretching(I2C1);
    i2c_set_7bit_addr_mode(I2C1);
    i2c_peripheral_enable(I2C1);
}
*/

static void gpio_init(void) {

    // pa0:   1 vc
    // pa1:   2 cura
    // pa2:   3 curb
    // pa3:   4 curc
    // pc0:   6 vb
    // pc2:   8 va
    GPIO_OUT(A, GPIO4, NONE);                          // pa4:    spi3 cs
    gpio_set(GPIOA, GPIO4);                            //
    GPIO_OPT(A, GPIO4, PP, 100MHZ);                    //
    GPIO_ALT(A, GPIO6, NONE, AF6);                     // pa6:    fault
    GPIO_ALT(A, GPIO8 | GPIO9 | GPIO10, NONE, AF6);    // pa8-10: ha,hb,hc
    GPIO_OUT(A, GPIO11, PULLUP);                       // pa11:   ecat reset
    GPIO_OPT(A, GPIO11, OD, 100MHZ);                   //
    gpio_set(GPIOA, GPIO11);                           //
                                                       //
    GPIO_ALT(B, GPIO0 | GPIO1, NONE, AF6);             // pb0-1:  lb,lc
    GPIO_ALT(B, GPIO3 | GPIO5, NONE, AF6);             // pb3,5:  spi3 sck,mosi
    GPIO_ALT(B, GPIO4, PULLUP, AF6);                   // pb4:    spi3 miso
    GPIO_ALT(B, GPIO6 | GPIO7, NONE, AF4);             // pb6,7:  i2c1 scl,sda
    GPIO_ALT(B, GPIO8 | GPIO9, NONE, AF7);             // pb8,9:  uart3 tx, rx
    GPIO_OPT(B, GPIO9, PP, 100MHZ);                    //
    GPIO_OUT(B, GPIO12, NONE);                         // pb12:   ecat cs
    GPIO_ALT(B, GPIO13 | GPIO15, NONE, AF5);           // pb13,15:ecat sck,mosi
    GPIO_ALT(B, GPIO14, PULLUP, AF5);                  // pb14:   ecat miso
    GPIO_OPT(B, GPIO12 | GPIO13 | GPIO15, PP, 100MHZ); //
                                                       //
    GPIO_INP(C, GPIO6, PULLUP);                        // pc6:    ecat irq
    GPIO_INP(C, GPIO7 | GPIO8, NONE);                  // pc7,8:  ecat sync0,1
    GPIO_OUT(C, GPIO9, NONE);                          // pc9:    led
    GPIO_OUT(C, GPIO10, NONE);                         // pc10:   ecat wake
    GPIO_OUT(C, GPIO11, NONE);                         // pc11:   enc cs
    GPIO_INP(C, GPIO12, NONE);                         // pc12:   pwrgd
    GPIO_ALT(C, GPIO13, NONE, AF4);                    // pb13:   la
                                                       //
    GPIO_OUT(D, GPIO2, NONE);                          // pd2:    en_gate
    gpio_clear(GPIOD, GPIO2);

    // IRQ pin handler
    nvic_enable_irq(NVIC_EXTI9_5_IRQ);
    exti_select_source(EXTI6, GPIOC);
    exti_set_trigger(EXTI6, EXTI_TRIGGER_BOTH);
    exti_enable_request(EXTI6);
}

static void adc_init(void) {
    adc_power_off(ADC1);
    adc_enable_regulator(ADC1);
    msleep(1);

    adc_set_clk_prescale(ADC1, ADC_CCR_CKMODE_DIV2);
    adc_set_single_conversion_mode(ADC1);
    adc_disable_external_trigger_regular(ADC1);
    adc_set_right_aligned(ADC1);
    adc_enable_temperature_sensor();
    adc_enable_vrefint();
    adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_61DOT5CYC);
    uint8_t channel_array[] = {16, 18}; // temp, vref
    adc_set_regular_sequence(ADC1, sizeof(channel_array), channel_array);
    adc_set_resolution(ADC1, ADC_CFGR1_RES_12_BIT);

    adc_calibrate(ADC1);
    adc_power_on(ADC1);
}

void board_set_led(bool on) {
    if (on) {
        gpio_set(GPIOC, GPIO9);
    } else {
        gpio_clear(GPIOC, GPIO9);
    }
}

volatile uint32_t system_millis;

void sys_tick_handler(void) {
    system_millis++;
}

void msleep(uint32_t delay) {
    uint32_t wake = system_millis + delay;
    while (wake > system_millis)
        ;
}

uint32_t adc_to_vref_mv(uint32_t adc) {
    return 3300 * (uint32_t)(ST_VREFINT_CAL) / adc;
}

uint32_t adc_to_temp_c(uint32_t adc) {
    const int32_t ts30  = ST_TSENSE_CAL1_30C;
    const int32_t ts110 = ST_TSENSE_CAL2_110C;
    const float d_tref  = 110.f - 30.f;
    const float t_sref  = ts110 - ts30;

    return (float)((int32_t)adc - ts30) / t_sref * d_tref + 30.f;
}

static void systick_init(void) {
    systick_set_reload(rcc_ahb_frequency / 1000);
    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);
    systick_counter_enable();
    systick_interrupt_enable();
}

static void timer_init(void) {
    nvic_enable_irq(NVIC_TIM2_IRQ);
    rcc_periph_reset_pulse(RST_TIM2);
    timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
    timer_set_prescaler(TIM2, 20);
    timer_disable_preload(TIM2);
    timer_continuous_mode(TIM2);
    timer_set_period(TIM2, 65535);

    timer_set_oc_value(TIM2, TIM_OC1, 8000);

    timer_enable_counter(TIM2);
    timer_enable_irq(TIM2, TIM_DIER_CC1IE);
}

void board_init(void) {
    clock_init();
    gpio_init();
    spi_init();
    // i2c_init();
    systick_init();
    adc_init();
    timer_init();
}
