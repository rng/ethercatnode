#include "ethercat.h"
#include "common.h"
#include "ethercat_hw.h"

#include "soes/esc.h"
#include "soes/esc_coe.h"

/**
 * EtherCAT Memory map:
 *
 * start   size  usage
 * -----------------------------------
 * 0x1000  128   in mailbox
 * 0x1080  128   out mailbox
 * 0x1100  128   rx (master->node) PDO
 * 0x1180  128   tx (node->master) PDO
 * ...
 * 0x1400  4     firmware version
 **/

#define EC_PRAM_FW_VERSION (0x1400)

#define MBXSIZE 128
#define MBXBUFFERS 3

// SM0 128 byte mailbox
#define MBX0_SM_addr 0x1000
#define MBX0_SM_len MBXSIZE
#define MBX0_SM_end MBX0_SM_addr + MBX0_SM_len - 1
#define MBX0_SM_cfg 0x26

// SM1 128 byte mailbox
#define MBX1_SM_addr 0x1080
#define MBX1_SM_len MBXSIZE
#define MBX1_SM_end MBX1_SM_addr + MBX1_SM_len - 1
#define MBX1_SM_cfg 0x22

// SM2 128 byte RX PDO
#define SM2_SM_addr 0x1100
#define SM2_SM_cfg 0x24

// SM3 128 byte TX PDO
#define SM3_SM_addr 0x1180
#define SM3_SM_cfg 0x20

_ESCvar ESCvar;
_MBXcontrol MBXcontrol[MBXBUFFERS];
uint8_t MBX[MBXBUFFERS * MBXSIZE];

ethercat_cfg_t ethercat_cfg;

// COE stubs

uint16_t sizeOfPDO(uint16_t index) {
    switch (index) {
        case TX_PDO_OBJIDX:
            return ethercat_cfg.txpdo_len;
        case RX_PDO_OBJIDX:
            return ethercat_cfg.rxpdo_len;
        default:
            abort();
            break;
    }
}

int COE_getSyncMgrPara(uint16_t index, uint8_t subindex, void *buf,
                       uint16_t datatype) {
    (void)index;
    (void)subindex;
    (void)buf;
    (void)datatype;
    return 0;
}

void COE_initDefaultSyncMgrPara(void) {
}

//

void APP_safeoutput(void) {
    ethercat_cfg.stop();
}

static void ethercat_state_change_hook(uint8_t *as, uint8_t *an) {
    (void)an;
    ethercat_state_t to_state   = (*as) >> 4;
    ethercat_state_t from_state = (*as) & 0xf;

    if (from_state != to_state) {
        ethercat_cfg.state_change(from_state, to_state);
    }
}

static esc_cfg_t esc_config = {
    .user_arg      = NULL,
    .use_interrupt = 0,
    .watchdog_cnt  = 0,
    .mbxsize       = MBXSIZE,
    .mbxsizeboot   = MBXSIZE,
    .mbxbuffers    = MBXBUFFERS,
    .mb[0]         = {MBX0_SM_addr, MBX0_SM_len, MBX0_SM_end, MBX0_SM_cfg, 0},
    .mb[1]         = {MBX1_SM_addr, MBX1_SM_len, MBX1_SM_end, MBX1_SM_cfg, 0},
    .mb_boot[0]    = {MBX0_SM_addr, MBX0_SM_len, MBX0_SM_end, MBX0_SM_cfg, 0},
    .mb_boot[1]    = {MBX1_SM_addr, MBX1_SM_len, MBX1_SM_end, MBX1_SM_cfg, 0},
    .pdosm[0]      = {SM2_SM_addr, 0, 0, SM2_SM_cfg, 1},
    .pdosm[1]      = {SM3_SM_addr, 0, 0, SM3_SM_cfg, 1},
    .post_state_change_hook = ethercat_state_change_hook,
};

void ethercat_init(ethercat_cfg_t *cfg) {
    ethercat_cfg = *cfg;

    ESCvar.TXPDOsize = ESCvar.ESC_SM3_sml = sizeOfPDO(TX_PDO_OBJIDX);
    ESCvar.RXPDOsize = ESCvar.ESC_SM2_sml = sizeOfPDO(RX_PDO_OBJIDX);

    ESC_config(&esc_config);
    ESC_init(&esc_config);

    while ((ESCvar.DLstatus & 0x0001) == 0) {
        ESC_read(ESCREG_DLSTATUS, (void *)&ESCvar.DLstatus,
                 sizeof(ESCvar.DLstatus));
    }

    // Init FoE
    FOE_config(ethercat_cfg.foe, ethercat_cfg.foe->files);
    FOE_init();

    ESC_ALstatus(ESCinit);
    ESC_ALerror(ALERR_NONE);
    ESC_stopmbx();
    ESC_stopinput();
    ESC_stopoutput();

    // write device info to EC PRAM
    ESC_write(EC_PRAM_FW_VERSION, &ethercat_cfg.fw_version,
              sizeof(ethercat_cfg.fw_version));
}

static void ethercat_pdo_process(void) {
    if (ESCvar.App.state & APPSTATE_OUTPUT) {
        if (ESCvar.ALevent & ESCREG_ALEVENT_SM2) {
            ESCvar.ALevent &= ~ESCREG_ALEVENT_SM2;
            // process data from master
            ASSERT(ethercat_cfg.rxpdo);
            ESC_read(SM2_SM_addr, ethercat_cfg.rxpdo, ESCvar.RXPDOsize);
        }
    }

    if (ethercat_cfg.tick) {
        ethercat_cfg.tick();
    }

    if (ESCvar.App.state) {
        // send data to master
        ASSERT(ethercat_cfg.txpdo);
        ESC_write(SM3_SM_addr, ethercat_cfg.txpdo, ESCvar.TXPDOsize);
    }
}

void ethercat_tick_once(void) {
    ESC_read(ESCREG_LOCALTIME, (void *)&ESCvar.Time, sizeof(ESCvar.Time));

    // Check the state machine
    ESC_state();
    // Check the SM activation event
    ESC_sm_act_event();

    // Check mailboxes
    if (ESC_mbxprocess()) {
        ESC_foeprocess();
        ESC_xoeprocess();
    }

    // Process PDOs
    ethercat_pdo_process();
}

void ethercat_tick(void) {
    ESC_int_clear();
    do {
        ethercat_tick_once();
    } while (ESCvar.ALevent);
}
