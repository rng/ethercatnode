#pragma once

#include <stdint.h>

typedef enum {
    A1335_REG_EWA   = 0x02, // (2) Extended Write Address
    A1335_REG_EWD   = 0x04, // (4) Extended Write Data
    A1335_REG_EWCS  = 0x08, // (2) Extended Write Control and Status
    A1335_REG_ERA   = 0x0a, // (2) Extended Read Address
    A1335_REG_ERCS  = 0x0c, // (2) Extended Read Control and Status
    A1335_REG_ERD   = 0x0e, // (4) Extended Read Data
    A1335_REG_CTRL  = 0x1e, // (2) Device control
    A1335_REG_ANG   = 0x20, // (2) Current angle and related data
    A1335_REG_STA   = 0x22, // (2) Device status
    A1335_REG_ERR   = 0x24, // (2) Device error status
    A1335_REG_XERR  = 0x26, // (2) Extended error status
    A1335_REG_TSEN  = 0x28, // (2) Temperature sensor data
    A1335_REG_FIELD = 0x2a, // (2) Magnetic field strength
    A1335_REG_ERM   = 0x34, // (2) Device error status masking
    A1335_REG_XERM  = 0x36, // (2) Extended error status masking
} a1335_reg_t;

typedef enum {
    A1335_STATUS_OK = 0,
    A1335_STATUS_ERR_GENERIC,
    A1335_STATUS_ERR_I2C,
} a1335_result_t;

uint16_t a1335_reg_read16(a1335_reg_t reg, a1335_result_t *err);
void a1335_reg_write16(a1335_reg_t reg, uint16_t value, a1335_result_t *err);

void a1335_clear_error(void);
uint16_t a1335_angle_read(a1335_result_t *err);
int16_t a1335_temp_read(a1335_result_t *err);
uint16_t a1335_field_read(a1335_result_t *err);
