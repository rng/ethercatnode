#include "drv8305.h"
#include "board.h"
#include "common.h"
#include "ulog.h"

#define _CMD_WRITE (0 << 7)
#define _CMD_READ (1 << 7)
#define _CMD_ADDR_SHIFT (3)
#define _CMD_VALUE_MASK (0x7ff)

uint16_t drv8305_reg_read16(drv8305_reg_t reg) {
    uint8_t cmd[2] = {_CMD_READ | (reg << _CMD_ADDR_SHIFT), 0};
    uint8_t resp[2];

    gpio_clear(GPIOA, GPIO4);
    for (uint32_t i = 0; i < sizeof(cmd); i++) {
        spi_send8(DRV_SPI, cmd[i]);
        resp[i] = spi_read8(DRV_SPI);
    }
    gpio_set(GPIOA, GPIO4);

    return ((resp[0] << 8) | resp[1]) & _CMD_VALUE_MASK;
}

void drv8305_reg_write16(drv8305_reg_t reg, uint16_t value) {
    value &= _CMD_VALUE_MASK;
    uint8_t cmd[2] = {_CMD_WRITE | (reg << _CMD_ADDR_SHIFT) | (value >> 8),
                      value & 0xff};

    gpio_clear(GPIOA, GPIO4);
    for (uint32_t i = 0; i < sizeof(cmd); i++) {
        spi_send8(DRV_SPI, cmd[i]);
        spi_read8(DRV_SPI);
    }
    gpio_set(GPIOA, GPIO4);
}

void drv8305_reg_dump(void) {
    static const char *regnames[] = {
        "WARN_WDOG", "OV_FAULT",  "IC_FAULT", "VGS_FAULT",
        "HS_GT_DRV", "LS_GT_DRV", "GATE_DRV", "----",
        "IC_OPER",   "IC_SNTAMP", "IC_VREG",  "IC_VDS_S",
    };
    ulog_printf("DRV8305 Regdump:\n");
    for (int i = 1; i <= 0xc; i++) {
        if (i == 8)
            continue;
        ulog_printf("  %9s [0x%x]: %04x\n", regnames[i - 1], i,
                    drv8305_reg_read16(i));
    }
}

void drv8305_init(drv8305_err_t *err) {
    drv8305_clear_fault();
    msleep(10);
    *err = drv8305_is_fault() ? DRV8305_ERR_FAULT : DRV8305_ERR_OK;
}

bool drv8305_is_fault(void) {
    uint16_t val = drv8305_reg_read16(DRV8305_REG_WARN_WDOG);
    return val & DRV8305_WARN_WDOG_FAULT;
    /*
    uint32_t val = 0;
    val |= drv8305_reg_read16(DRV8305_REG_WARN_WDOG, &err);
    val |= drv8305_reg_read16(DRV8305_REG_OV_FAULT, &err);
    val |= drv8305_reg_read16(DRV8305_REG_IC_FAULT, &err);
    val |= drv8305_reg_read16(DRV8305_REG_VGS_FAULT, &err);
    return val != 0;
    */
}

void drv8305_clear_fault(void) {
    uint16_t v = DRV8305_IC_OPER_FLIP_OTSD | DRV8305_IC_OPER_EN_SNS_CLAMP |
                 DRV8305_IC_OPER_CLR_FLTS;
    drv8305_reg_write16(DRV8305_REG_IC_OPER, v);
}

void drv8305_enable(bool enable) {
    if (enable) {
        gpio_set(GPIOD, GPIO2);
    } else {
        gpio_clear(GPIOD, GPIO2);
    }
}
