#include "flash.h"
#include "common.h"

void flash_wait_for_last_operation(void) {
    while ((FLASH_SR & FLASH_SR_BSY) == FLASH_SR_BSY)
        ;
}

void flash_program_half_word(uint32_t address, uint16_t data) {
    flash_wait_for_last_operation();

    FLASH_CR |= FLASH_CR_PG;
    MMIO16(address) = data;

    flash_wait_for_last_operation();

    ASSERT(FLASH_SR == FLASH_SR_EOP);
    flash_clear_eop_flag();
    FLASH_CR &= ~FLASH_CR_PG;
}

void flash_erase_page(uint32_t page_address) {
    flash_wait_for_last_operation();

    FLASH_CR |= FLASH_CR_PER;
    FLASH_AR = page_address;
    FLASH_CR |= FLASH_CR_STRT;

    flash_wait_for_last_operation();

    ASSERT(FLASH_SR == FLASH_SR_EOP);
    flash_clear_eop_flag();
    FLASH_CR &= ~FLASH_CR_PER;
}

void flash_erase_partition(uint32_t partition) {
    ASSERT(flash_active_partition() != partition);
    ASSERT((partition % FLASH_PAGE_SIZE) == 0);

    uint32_t addr          = partition;
    uint32_t partition_end = partition + PARTITION_SIZE;
    while (addr < partition_end) {
        flash_erase_page(addr);
        addr += FLASH_PAGE_SIZE;
    }
}

uint32_t flash_active_partition(void) {
    return ((uint32_t)flash_active_partition) < B_PARTITION ? A_PARTITION
                                                            : B_PARTITION;
}
