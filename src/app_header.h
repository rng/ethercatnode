#pragma once

#include <stdint.h>

#define APP_MAGIC_ID (0x54414345) // 'ECAT'
#define APP_VERSION (1)

typedef struct {
    uint32_t magic;
    uint32_t buildtime;
    uint32_t buildversion;
    char buildsha[12];
} app_header_t;
