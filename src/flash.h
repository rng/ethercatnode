#pragma once

#include <stdint.h>
#include "libopencm3/stm32/flash.h"

#define FLASH_START (0x08000000)

#define PARTITION_SIZE (30 * 1024)
#define A_PARTITION (FLASH_START + (4 * 1024))
#define B_PARTITION (A_PARTITION + PARTITION_SIZE)

#define FLASH_PAGE_SIZE (1024 * 2)

void flash_program_half_word(uint32_t address, uint16_t data);
void flash_erase_page(uint32_t page_address);
void flash_erase_partition(uint32_t partition);
uint32_t flash_active_partition(void);
