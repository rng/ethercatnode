#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef enum {
    DRV8305_REG_WARN_WDOG   = 0x1,
    DRV8305_REG_OV_FAULT    = 0x2,
    DRV8305_REG_IC_FAULT    = 0x3,
    DRV8305_REG_VGS_FAULT   = 0x4,
    DRV8305_REG_HS_GATE_DRV = 0x5,
    DRV8305_REG_LS_GATE_DRV = 0x6,
    DRV8305_REG_GATE_DRV    = 0x7,
    // RESERVED
    DRV8305_REG_IC_OPER      = 0x9,
    DRV8305_REG_IC_SHUNT_AMP = 0xA,
    DRV8305_REG_IC_VREG      = 0xB,
    DRV8305_REG_IC_VDS_SENSE = 0xC,
} drv8305_reg_t;

// Reg 0x1: Warning/Watchdog
#define DRV8305_WARN_WDOG_FAULT (1 << 10)
#define DRV8305_WARN_WDOG_TEMP4 (1 << 8)
#define DRV8305_WARN_WDOG_PVDD_UVFL (1 << 7)
#define DRV8305_WARN_WDOG_PVDD_OVFL (1 << 6)
#define DRV8305_WARN_WDOG_VDS_STATUS (1 << 5)
#define DRV8305_WARN_WDOG_VCHP_UVFL (1 << 4)
#define DRV8305_WARN_WDOG_TEMP1 (1 << 3)
#define DRV8305_WARN_WDOG_TEMP2 (1 << 2)
#define DRV8305_WARN_WDOG_TEMP3 (1 << 1)
#define DRV8305_WARN_WDOG_OTW (1 << 0)

// Reg 0x2: OV/VDS Faults
#define DRV8305_OV_FAULT_VDS_HA (1 << 10)
#define DRV8305_OV_FAULT_VDS_LA (1 << 9)
#define DRV8305_OV_FAULT_VDS_HB (1 << 8)
#define DRV8305_OV_FAULT_VDS_LB (1 << 7)
#define DRV8305_OV_FAULT_VDS_HC (1 << 6)
#define DRV8305_OV_FAULT_VDS_LC (1 << 5)
#define DRV8305_OV_FAULT_SNS_C (1 << 2)
#define DRV8305_OV_FAULT_SNS_B (1 << 1)
#define DRV8305_OV_FAULT_SNS_A (1 << 0)

// Reg 0x3: IC Faults
#define DRV8305_IC_FAULT_PVDD_UVLO (1 << 10)
#define DRV8305_IC_FAULT_WD_FAULT (1 << 9)
#define DRV8305_IC_FAULT_OTSD (1 << 8)
#define DRV8305_IC_FAULT_VREG_UV (1 << 6)
#define DRV8305_IC_FAULT_AVDD_UVLO (1 << 5)
#define DRV8305_IC_FAULT_VCP_UVLO (1 << 4)
#define DRV8305_IC_FAULT_VCPH_UVLO (1 << 2)
#define DRV8305_IC_FAULT_VCPH_OVLO (1 << 1)
#define DRV8305_IC_FAULT_VCPH_OVLO_A (1 << 0)

// Reg 0x4: VGS Faults
#define DRV8305_VGS_FAULT_VGS_HA (1 << 10)
#define DRV8305_VGS_FAULT_VGS_LA (1 << 9)
#define DRV8305_VGS_FAULT_VGS_HB (1 << 8)
#define DRV8305_VGS_FAULT_VGS_LB (1 << 7)
#define DRV8305_VGS_FAULT_VGS_HC (1 << 6)
#define DRV8305_VGS_FAULT_VGS_LC (1 << 5)

// Reg 0x9: IC Operation
#define DRV8305_IC_OPER_FLIP_OTSD (1 << 10)
#define DRV8305_IC_OPER_DS_PVDD_UVLO (1 << 9)
#define DRV8305_IC_OPER_DS_GDRV_FLT (1 << 8)
#define DRV8305_IC_OPER_EN_SNS_CLAMP (1 << 7)

#define DRV8305_IC_OPER_WD_DLY_100ms (3 << 5)
#define DRV8305_IC_OPER_WD_DLY_50ms (2 << 5)
#define DRV8305_IC_OPER_WD_DLY_20ms (1 << 5)
#define DRV8305_IC_OPER_WD_DLY_10ms (0 << 5)

#define DRV8305_IC_OPER_DS_SNS_OCP (1 << 4)
#define DRV8305_IC_OPER_WD_EN (1 << 3)
#define DRV8305_IC_OPER_SLEEP (1 << 2)
#define DRV8305_IC_OPER_CLR_FLTS (1 << 1)
#define DRV8305_IC_OPER_SET_VC (1 << 0)

typedef enum {
    DRV8305_ERR_OK = 0,
    DRV8305_ERR_FAULT,
} drv8305_err_t;

uint16_t drv8305_reg_read16(drv8305_reg_t reg);
void drv8305_reg_write16(drv8305_reg_t reg, uint16_t value);
void drv8305_reg_dump(void);

void drv8305_init(drv8305_err_t *err);
void drv8305_enable(bool enable);
bool drv8305_is_fault(void);
void drv8305_clear_fault(void);
